const
	test = require('ava'),
	{Form} = require('../src/Form');

test('Set/get data', t => {
	let
		form, got,
		formEl = createForm();

	form = new Form({
		form: formEl,
		handlers: {
			save: () => {}
		}
	});

	// set not null values
	form.setData({
		text: 'text',
		checkbox: true,
		radio: 'radio2',
		longtext: 'longtext',
		select: 'opt2',
		selectMulti: ['opt1', 'opt2']
	});
	got = form.getData();

	t.is(got.text, 'text');
	t.is(got.checkbox, true);
	t.true(got.file instanceof FileList);
	t.true(got.files instanceof FileList);
	t.is(got.radio, 'radio2');
	t.is(got.longtext, 'longtext');
	t.is(got.select, 'opt2');
	t.deepEqual(got.selectMulti, ['opt1', 'opt2']);

	// change some fields
	form.setData({
		text: '2',
		radio: 'radio1',
		checkbox: false,
		selectMulti: 'opt2'
	});
	got = form.getData();

	t.is(got.text, 2);
	t.is(got.checkbox, false);
	t.true(got.file instanceof FileList);
	t.true(got.files instanceof FileList);
	t.is(got.radio, 'radio1');
	t.is(got.longtext, 'longtext');
	t.is(got.select, 'opt2');
	t.deepEqual(got.selectMulti, ['opt2']);

	// clear fields
	form.setData({
		text: '',
		file: undefined,
		radio: undefined,
		checkbox: undefined,
		select: undefined,
		selectMulti: ''
	});
	got = form.getData();

	t.is(got.text, undefined);
	t.is(got.checkbox, false);
	t.true(got.file instanceof FileList);
	t.true(got.files instanceof FileList);
	t.is(got.radio, undefined);
	t.is(got.longtext, 'longtext');
	t.is(got.select, 'opt1');
	t.deepEqual(got.selectMulti, ['opt1']);
});

test('Initial data', t => {
	let
		form, got,
		formEl = createForm();

	form = new Form({
		form: formEl,
		handlers: {
			save: () => {}
		}
	});
	got = form.getData();

	t.is(got.text, 'text');
	t.is(got.checkbox, true);
	t.true(got.file instanceof FileList);
	t.true(got.files instanceof FileList);
	t.is(got.radio, 'radio2');
	t.is(got.longtext, 'longtext');
	t.is(got.select, 'opt2');
	t.deepEqual(got.selectMulti, ['opt1', 'opt2']);
});

test('Type cast', t => {
	let
		form,
		formEl = createForm();

	form = new Form({
		form: formEl,
		handlers: {
			save: () => {}
		}
	});

	form.setData({
		text: '2'
	});
	t.is(form.getData().text, 2);

	form.setData({
		text: 2
	});
	t.is(form.getData().text, 2);

	form.setData({
		text: '2.2'
	});
	t.is(form.getData().text, 2.2);

	form.setData({
		text: '2,2'
	});
	t.is(form.getData().text, 2.2);

	form.setData({
		text: 2.2
	});
	t.is(form.getData().text, 2.2);

	form.setData({
		text: '0'
	});
	t.is(form.getData().text, 0);

	form.setData({
		text: '00'
	});
	t.is(form.getData().text, '00');

	form.setData({
		text: '001'
	});
	t.is(form.getData().text, '001');

	form.setData({
		text: '101'
	});
	t.is(form.getData().text, 101);

	form.setData({
		text: 101
	});
	t.is(form.getData().text, 101);

	form.setData({
		text: '0.01'
	});
	t.is(form.getData().text, 0.01);

	form.setData({
		text: '0,01'
	});
	t.is(form.getData().text, 0.01);

	form.setData({
		text: 0.01
	});
	t.is(form.getData().text, 0.01);

	form.setData({
		text: 1.0
	});
	t.is(form.getData().text, 1);

	form.setData({
		text: '1.0'
	});
	t.is(form.getData().text, 1);

	form.setData({
		text: '-0,01'
	});
	t.is(form.getData().text, -0.01);

	form.setData({
		text: -1
	});
	t.is(form.getData().text, -1);
});

test('Dirty state', t => {
	let
		form,
		formEl = createForm();

	form = new Form({
		form: formEl,
		handlers: {
			save: () => {}
		}
	});

	t.false(form.isDirty());
	formEl.querySelector('[name="text"]').value = 'newText';
	t.true(form.isDirty());
});

test('Preserve readonly state when toggling edit mode', t => {
	let
		form,
		formEl = document.createElement('form');

	formEl.insertAdjacentHTML('afterbegin', `
		<input type="text" name="textNotEditable" class="field" readonly="readonly"/>
		<input type="text" name="textEditable" class="field" />
	`);

	form = new Form({
		form: formEl,
		persistentEditMode: false,
		handlers: {
			save: () => {}
		}
	});

	t.true(formEl.querySelector('[name="textNotEditable"]').hasAttribute('readonly'));
	t.true(formEl.querySelector('[name="textEditable"]').hasAttribute('readonly'));

	form.edit();

	t.true(formEl.querySelector('[name="textNotEditable"]').hasAttribute('readonly'));
	t.false(formEl.querySelector('[name="textEditable"]').hasAttribute('readonly'));

	form.cancel();

	t.true(formEl.querySelector('[name="textNotEditable"]').hasAttribute('readonly'));
	t.true(formEl.querySelector('[name="textEditable"]').hasAttribute('readonly'));
});

test('Validators', async t => {
	let
		form,
		formEl = document.createElement('form');

	form = new Form({
		form: formEl,
		handlers: {
			save: () => {}
		}
	});

	// single validator - 'required'
	formEl.innerHTML = `
		<input type="text" name="name" class="field" data-validators="required"/>
	`;
	form.resetValidation();
	await form.save()
		.then(valid => {
			t.false(valid);
		});
	formEl.querySelector('input').value = 'a';
	form.resetValidation();
	await form.save()
		.then(valid => {
			t.true(valid);
		});

	// multiple validators - 'required' and 'email'
	formEl.innerHTML = `
		<input type="text" name="name" class="field" data-validators="required email"/>
	`;
	form.resetValidation();
	await form.save()
		.then(valid => {
			t.false(valid);
		});
	formEl.querySelector('input').value = 'a';
	form.resetValidation();
	await form.save()
		.then(valid => {
			t.false(valid);
		});
	formEl.querySelector('input').value = 'aa@aa.aa';
	form.resetValidation();
	await form.save()
		.then(valid => {
			t.true(valid);
		});

	// 'passwordConfirm' validator
	formEl.innerHTML = `
		<input type="password" name="pass1" class="field"/>
		<input type="password" name="pass2" class="field" data-validators="passwordConfirm"/>
	`;
	form.resetValidation();
	await form.save()
		.then(valid => {
			t.false(valid);
		});
	formEl.querySelector('[name="pass2"]').setAttribute('data-related-field', 'pass1');
	form.resetValidation();
	await form.save()
		.then(valid => {
			t.true(valid);
		});
	formEl.querySelector('[name="pass1"]').value = 'a';
	form.resetValidation();
	await form.save()
		.then(valid => {
			t.false(valid);
		});
	formEl.querySelector('[name="pass2"]').value = 'a';
	form.resetValidation();
	await form.save()
		.then(valid => {
			t.true(valid);
		});

	// 'checked' validator
	formEl.innerHTML = `
		<input type="checkbox" name="name" class="field" data-validators="checked"/>
	`;
	form.resetValidation();
	await form.save()
		.then(valid => {
			t.false(valid);
		});
	formEl.querySelector('input').checked = true;
	form.resetValidation();
	await form.save()
		.then(valid => {
			t.true(valid);
		});

	// 'unchecked' validator
	formEl.innerHTML = `
		<input type="checkbox" name="name" class="field" data-validators="unchecked"/>
	`;
	form.resetValidation();
	await form.save()
		.then(valid => {
			t.true(valid);
		});
	formEl.querySelector('input').checked = true;
	form.resetValidation();
	await form.save()
		.then(valid => {
			t.false(valid);
		});

	// custom validator doesn't exist
	formEl.innerHTML = `
		<input type="text" name="name" class="field" data-validators="custom"/>
	`;
	form.resetValidation();
	await form.save()
		.then(valid => {
			t.true(valid);
		});

	// custom validator
	form = new Form({
		form: formEl,
		handlers: {
			save: () => {}
		},
		validators: {
			custom: (value, field) => {
				return {
					valid: value.length && field.classList.contains('custom-validator')
				};
			}
		}
	});
	formEl.innerHTML = `
		<input type="text" name="name" class="field" data-validators="custom"/>
	`;
	form.resetValidation();
	await form.save()
		.then(valid => {
			t.false(valid);
		});
	formEl.querySelector('input').value = 'a';
	formEl.querySelector('input').classList.add('custom-validator');
	form.resetValidation();
	await form.save()
		.then(valid => {
			t.true(valid);
		});
});

test('Object comparison', t => {
	let
		a, b, c, form,
		formEl = createForm();

	form = new Form({
		form: formEl,
		handlers: {
			save: () => {}
		}
	});

	a = {};
	b = {};
	t.true(form._compareObjects(a, b));
	t.true(form._compareObjects(a, b, true));

	// string
	a = {
		a: 'a'
	};
	b = {
		a: 'a'
	};
	c = {
		a: 2
	};
	t.true(form._compareObjects(a, b));
	t.true(form._compareObjects(a, b, true));
	t.false(form._compareObjects(a, c));
	t.false(form._compareObjects(a, c, true));

	// number
	a = {
		a: 2
	};
	b = {
		a: 2
	};
	c = {
		a: '2'
	};
	t.true(form._compareObjects(a, b));
	t.true(form._compareObjects(a, b, true));
	t.true(form._compareObjects(a, c));
	t.false(form._compareObjects(a, c, true));

	// boolean
	a = {
		a: false
	};
	b = {
		a: false
	};
	c = {
		a: 'false'
	};
	t.true(form._compareObjects(a, b));
	t.true(form._compareObjects(a, b, true));
	t.false(form._compareObjects(a, c));
	t.false(form._compareObjects(a, c, true));

	// array
	a = {
		a: [2]
	};
	b = {
		a: ['2']
	};
	c = {
		a: ['a']
	};
	t.true(form._compareObjects(a, b));
	t.false(form._compareObjects(a, b, true));
	t.false(form._compareObjects(a, c));
	t.false(form._compareObjects(a, c, true));

	// object
	a = {
		a: {
			a: 2
		}
	};
	b = {
		a: {
			a: '2'
		}
	};
	c = {
		a: {
			a: 'a'
		}
	};
	t.true(form._compareObjects(a, b));
	t.false(form._compareObjects(a, b, true));
	t.false(form._compareObjects(a, c));
	t.false(form._compareObjects(a, c, true));
});

function createForm() {
	let
		formEl = document.createElement('form');

	formEl.insertAdjacentHTML('afterbegin', `
		<input type="text" name="text" class="field" value="text"/>
		<input type="checkbox" name="checkbox" class="field" checked="checked"/>
		<input type="radio" name="radio" value="radio1" class="field"/>
		<input type="radio" name="radio" value="radio2" class="field" checked="checked"/>
		<input type="file" name="file" class="field"/>
		<input type="file" name="files" class="field" multiple="multiple"/>
		<textarea name="longtext" class="field">longtext</textarea>
		<select name="select" class="field">
			<option value="opt1"/>
			<option value="opt2" selected="selected"/>
		</select>
		<select name="selectMulti" class="field" multiple="multiple">
			<option value="opt1" selected="selected"/>
			<option value="opt2" selected="selected"/>
		</select>
	`);

	return formEl;
}
