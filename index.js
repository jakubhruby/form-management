const
	{Form} = require('./src/Form.js');

exports = {
	Form: Form
};

// CommonJS
module.exports = exports;
