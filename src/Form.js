const
	{Waterfall} = require('waterfall-exec'),
	{assert} = require('assert-utils');

class Form {

	/**
	 * Initiate form management
	 * @constructs
	 * @param {Object} config configuration
	 * @param {Element} config.form <form> element
	 * @param {Object} [config.handlers] event listeners
	 * @param {function} [config.handlers.save] is called when the form is submitted
	 * @param {function} [config.handlers.delete] is called when delete button is pressed
	 * @param {function} [config.handlers.change] is called when any field changes its value
	 * @param {function} [config.handlers.cancel] is called when edit is cancelled
	 * @param {string} [config.id] form identification
	 * @param {Object} [config.validators] validation functions dictionary, each in form validatorName(value, field) {return {valid: [boolean], hint: [string]}}
	 * @param {Object} [config.plugins] dictionary of special fields, each in form {init: fn(field) {}, getValue() {return value;}}
	 * @param {boolean} [config.editable=true] enable form editing
	 * @param {boolean} [config.editOnClick=false] enable edit mode by clicking somewhere on form
	 * @param {boolean} [config.persistentEditMode=true] edit mode is always enabled (standard form behavior)
	 * @param {boolean} [config.displayEdit=false] display edit button
	 * @param {boolean} [config.displayDelete=false] display delete button
	 * @param {function} [config.translate] translate function in form function(text) {return translatedText;}
	 * @param {boolean} [config.autoFocus=false] focus the first editable field on edit start
	 * @param {boolean} [config.validatePreviousOnFocus=true] validate previous fields on focus
	 * @param {boolean} [config.validateOnBlur=true] validate field on blur
	 * @returns {undefined}
	 */
	constructor(config) {
		assert.type(config, {
			form: 'element',
			handlers: {
				_required: false,
				save: '?function',
				delete: '?function',
				change: '?function',
				cancel: '?function'
			},
			id: '?string|number',
			validators: '?{}',
			plugins: '?{}',
			editable: '?boolean',
			editOnClick: '?boolean',
			persistentEditMode: '?boolean',
			displayEdit: '?boolean',
			displayDelete: '?boolean',
			translate: '?function',
			autoFocus: '?boolean',
			validatePreviousOnFocus: '?boolean',
			validateOnBlur: '?boolean'
		});

		this.form = config.form;
		this.handlers = config.handlers || {};
		this.id = config.id;
		this.validators = Object.assign({}, this.VALIDATORS, config.validators || {});
		this.plugins = config.plugins || {};
		this.editable = config.editable === undefined ? true : config.editable;
		this.editOnClick = config.editOnClick;
		this.persistentEditMode = config.persistentEditMode === undefined ? true : config.persistentEditMode;
		this.displayEdit = config.displayEdit;
		this.displayDelete = config.displayDelete;
		this.translate = config.translate;
		this.autoFocus = config.autoFocus;
		this.validatePreviousOnFocus = config.validatePreviousOnFocus === undefined ? true : config.validatePreviousOnFocus;
		this.validateOnBlur = config.validateOnBlur === undefined ? true : config.validateOnBlur;

		this.validatedValues = {};
		this.data = {};
		this.loadingMask = this.form.querySelector('.loading-mask');
		this.validationWaterfalls = [];

		this._getFields('[readonly]').forEach(field => {
			field.setAttribute('data-readonly', 'true');
		});

		if (this.persistentEditMode) {
			this.edit();
		}
		else {
			this.cancel();
		}

		for (let plugin in this.plugins) {
			assert.value(this._getFields(`[name="${plugin}"]`).length, 1);
			assert.type(this.plugins[plugin], {
				init: 'function',
				getValue: 'function'
			});

			this.plugins[plugin].init(this._getFields(`[name="${plugin}"]`));
		}

		this._bindFormHandlers();
		this._setInitialData(config.data);
	}

	/**
	 * Return form element
	 * @returns {Element}
	 */
	getEl() {
		return this.form;
	}

	/**
	 * Return initial form data
	 * @returns {Object}
	 */
	getOriginalData() {
		return this.data;
	}

	/**
	 * Returns current form data
	 * @returns {Object}
	 */
	getData() {
		let
			data = {};

		this._getFields().forEach(field => {
			let
				name = field.getAttribute('name');

			if (name in this.plugins) {
				data[name] = this.plugins[name].getValue();
			}
			else {
				if (field.matches('[type="file"]')) {
					data[name] = field.files;
				}
				else if (field.matches('[type="radio"]')) {
					if (field.checked) {
						let
							value = field.value;

						if (value.trim().match(/^-?([1-9][0-9]*|0)((\.|,)[0-9]+)?$/)) {
							value = parseFloat(value.replace(',', '.'));
						}
						data[name] = value;
					}
				}
				else if (field.matches('[type="checkbox"]')) {
					data[name] = field.checked;
				}
				else if (field.matches('select[multiple]')) {
					data[name] = [];
					for (let i = 0; i < field.options.length; i++) {
						if (field.options[i].selected) {
							let
								value = field.options[i].value;

							if (value.trim().match(/^-?([1-9][0-9]*|0)((\.|,)[0-9]+)?$/)) {
								value = parseFloat(value.replace(',', '.'));
							}

							data[name].push(value);
						}
					}
				}
				else {
					let
						value = field.value;

					if (value.trim().match(/^-?([1-9][0-9]*|0)((\.|,)[0-9]+)?$/)) {
						value = parseFloat(value.replace(',', '.'));
					}

					data[name] = value === '' ? undefined : value;
				}
			}
		});

		return data;
	}

	/**
	 * Returns custom ID
	 * @returns {(string|number)}
	 */
	getId() {
		return this.id;
	}

	/**
	 * Returns true if the current form data differs from initial data
	 * @returns {boolean}
	 */
	isDirty() {
		return !this._compareObjects(this.data, this.getData());
	}

	/**
	 * Sets editable form state
	 * @param {boolean} editable
	 * @returns {undefined}
	 */
	setEditable(editable) {
		assert.type(editable, 'boolean');

		if (editable != this.editable) {
			let
				buttons = this._getButtons();

			this.editable = editable;

			if (!editable) {
				this.cancel();
			}

			if (buttons.edit) {
				if (editable && this.displayEdit) {
					buttons.edit.style.display = null;
				}
				else {
					buttons.edit.style.display = 'none';
				}
			}
		}
	}

	/**
	 * Updates initial form data
	 * @param {Object} data
	 * @returns {undefined}
	 */
	setData(data) {
		assert.type(data, '{}');

		this._getFields().forEach(field => {
			let
				name = field.getAttribute('name'),
				value = data[name];

			if (typeof value == 'number') {
				value = value.toString();
			}

			if (name in data) {
				if (value === undefined) {
					value = '';
				}
				else if (field.matches('select') && !value) {
					let
						option = field.querySelector('option');

					value = option ? option.value : undefined;
				}
				else if (field.getAttribute('type') === 'checkbox' && typeof value === 'string') {
					value = value === 'on' ? true : false;
				}

				if (value !== undefined) {
					if (field.matches('[type="file"]')) {
						// only clear
						if (!data[name]) {
							value = '';
						}
					}
					else if (field.matches('[type="radio"]')) {
						field.checked = field.value == value;
					}
					else if (field.matches('[type="checkbox"]')) {
						field.checked = !!value;
					}
					else if (field.matches('select')) {
						let
							options = field.querySelectorAll('option');

						if (value instanceof Array) {
							value = value.map(val => {
								return val.toString();
							});
						}
						else {
							value = [value];
						}

						options.forEach(option => {
							option.selected = value.includes(option.value);
						});
					}
					else {
						field.value = value;
					}
				}

				this.data[name] = data[name] === '' ? undefined : data[name];
			}
		});
	}

	/**
	 * Updates custom ID
	 * @param {(string|number)} id
	 * @returns {undefined}
	 */
	setId(id) {
		assert.type(id, 'string|number');
		this.id = id;
	}

	/**
	 * Switches form to the EDIT mode
	 * @returns {undefined}
	 */
	edit() {
		let
			firstField,
			buttons = this._getButtons();

		this.form.classList.add('active');

		if (buttons.edit) {
			buttons.edit.style.display = 'none';
		}

		if (buttons.save) {
			buttons.save.style.display = null;
		}

		if (buttons.cancel) {
			buttons.cancel.style.display = this.persistentEditMode ? 'none' : null;
		}

		if (buttons.delete) {
			buttons.delete.style.display = this.displayDelete ? null : 'none';
		}

		this._getFields(':not([data-readonly])').forEach(field => {
			field.removeAttribute('readonly');
		});

		firstField = this._getFields(':not([readonly]):not([type="hidden"])')[0];

		if (firstField && this.autoFocus) {
			firstField.focus();
		}
	}

	/**
	 * Switches form to the READ mode, if it is allowed
	 * @param {boolean} [triggerCallback]
	 * @returns {undefined}
	 */
	cancel(triggerCallback) {
		assert.type(triggerCallback, '?boolean');

		if (!this.persistentEditMode) {
			let
				buttons = this._getButtons();

			this.form.classList.remove('active');

			this._getFields().forEach(field => {
				field.setAttribute('readonly', 'readonly');
			});

			if (buttons.edit) {
				buttons.edit.style.display = this.displayEdit ? null : 'none';
			}

			if (buttons.save) {
				buttons.save.style.display = 'none';
			}

			if (buttons.cancel) {
				buttons.cancel.style.display = 'none';
			}

			if (buttons.delete) {
				buttons.delete.style.display = 'none';
			}

			if (this.form.contains(document.activeElement)) {
				document.activeElement.blur();
				window.getSelection().empty();
			}
		}

		if (triggerCallback && this.handlers.cancel) {
			this.handlers.cancel();
		}

		this.reset();
	}

	/**
	 * Runs form validation, sets dirty state and displays loading mask if any
	 * @param {boolean} [triggerCallback]
	 * @returns {Promise}
	 */
	save(triggerCallback) {
		assert.type(triggerCallback, '?boolean');

		return this.validateForm()
			.then(validations => {
				let
					valid = validations.every(validation => {
						return validation;
					});

				if (valid) {
					this.dirty = false;
					this.showLoadingMask();
				}

				if (triggerCallback && this.handlers.save) {
					this.handlers.save(valid);
				}

				return valid;
			})
			.catch(reason => {
				console.error(reason);
			});
	}

	/**
	 * Resets form to initial state
	 * @returns {undefined}
	 */
	reset() {
		this.setData(this.data);
		this.hideLoadingMask();
		this.resetValidation();
		this.dirty = false;
	}

	/**
	 * Validates all form fields
	 * @returns {Promise}
	 */
	validateForm() {
		let
			pendingValidations = [];

		this._getFields('[data-validators]:not([readonly])').forEach(field => {
			pendingValidations.push(this.validateField(field));
		});
		return Promise.all(pendingValidations);
	}

	/**
	 * Validates a field
	 * @param {Element} field
	 * @returns {(boolean|Promise)}
	 */
	validateField(field) {
		let
			valid,
			labels = this._getLabels(field),
			fieldName = field.getAttribute('name'),
			validationValue = field.value,
			validationWaterfall = new Waterfall(),
			validators = (field.getAttribute('data-validators') || 'default').split(' ');

		this.validationWaterfalls.push(validationWaterfall);

		if (field.getAttribute('type') == 'checkbox') {
			validationValue = field.checked;
		}

		if (field.getAttribute('type') == 'radio') {
			let
				checkedField = this.form.querySelector('[name="' + field.getAttribute('name') + '"]:checked');

			if (checkedField) {
				validationValue = checkedField.value;
			}
			else {
				validationValue = undefined;
			}
		}

		if (validators.indexOf('passwordConfirm') >= 0) {
			let
				relatedField = this.form.querySelector(`[name="${field.getAttribute('data-related-field')}"]`);

			validationValue = {
				password: relatedField ? relatedField.value : undefined,
				passwordConfirm: field.value
			};
		}

		if ((fieldName in this.validatedValues) && JSON.stringify(this.validatedValues[fieldName].value) === JSON.stringify(validationValue)) {
			let
				validationResult = this.validatedValues[fieldName].valid;

			if (validationResult instanceof Promise) {
				valid = validationResult;
			}
			else {
				valid = Promise.resolve(validationResult);
			}
		}
		else {
			this.validatedValues[fieldName] = {
				value: validationValue
			};

			valid = validationWaterfall.exec(validators.map(validator => {
				return () => {
					let
						fieldValidation = (this.validators[validator] || this.validators.default).call(this, validationValue, field);

					if (typeof fieldValidation === 'function') {
						fieldValidation = fieldValidation();
					}

					if (!(fieldValidation instanceof Promise)) {
						fieldValidation = Promise.resolve(fieldValidation);
					}

					return fieldValidation
						.then(validationResult => {
							if (this.form.classList.contains('active')) {
								if (validationResult.valid || !this.form.classList.contains('active')) {
									if (field.type !== 'radio' || field.checked) {
										field.classList.add('valid');
									}
									field.classList.remove('invalid');

									if (labels.length) {
										labels.forEach(label => {
											if (field.type !== 'radio') {
												label.classList.add('valid');
											}
											label.classList.remove('invalid');
										});
									}

									if (field.type === 'radio') {
										let
											checkedInput = this.form.querySelector('[name="' + fieldName + '"]:checked');

										if (checkedInput) {
											let
												valueLabels = this.form.querySelectorAll('label[for="' + checkedInput.getAttribute('id') + '"]');

											valueLabels.forEach(label => {
												label.classList.add('valid');
											});
										}
									}
								}
								else {
									validationWaterfall.stop();
									field.classList.remove('valid');
									field.classList.add('invalid');

									if (labels.length) {
										labels.forEach(label => {
											label.classList.remove('valid');
											label.classList.add('invalid');

											if (validationResult.hint) {
												label.setAttribute('data-hint', validationResult.hint);
											}
											else {
												label.removeAttribute('data-hint');
											}
										});
									}
								}
								this.validatedValues[fieldName].valid = validationResult.valid;
								return validationResult.valid;
							}
							else {
								validationWaterfall.stop();
								return true;
							}
						});
				};
			}));

			this.validatedValues[fieldName].valid = valid;
		}

		return valid;
	}

	/**
	 * Resets form validation, also clears validation cache
	 * @param {string} [fieldName]
	 * @returns {undefined}
	 */
	resetValidation(fieldName) {
		if (fieldName) {
			let
				field = this.form.querySelector('[name="' + fieldName + '"]'),
				labels = this._getLabels(field);

			field.classList.remove('invalid');
			labels.forEach(label => {
				label.classList.remove('invalid');
			});
			delete this.validatedValues[fieldName];
		}
		else {
			this.form.querySelectorAll('.invalid').forEach(field => {
				field.classList.remove('valid');
				field.classList.remove('invalid');
			});
			this.validatedValues = {};
		}

		this.validationWaterfalls.forEach(validationWaterfall => {
			validationWaterfall.stop();
		});
	}

	/**
	 * Adds 'active' class to loading mask element, if exists
	 * @returns {undefined}
	 */
	showLoadingMask() {
		if (this.loadingMask) {
			assert.type(this.loadingMask, 'element');
			this.loadingMask.classList.add('active');
		}
	}

	/**
	 * Removes 'active' class from loading mask element, if exists
	 * @returns {undefined}
	 */
	hideLoadingMask() {
		if (this.loadingMask) {
			assert.type(this.loadingMask, 'element');
			this.loadingMask.classList.remove('active');
		}
	}

	/**
	 * Validates all fields that are preious to the focused one
	 * @param  {Element} field
	 * @returns {undefined}
	 */
	_validatePreviousFields(field) {
		let
			fieldVisited = false;

		// validate all previous fields
		this._getFields(':not([readonly])').forEach(item => {
			if (field === item) {
				fieldVisited = true;
			}

			if (!fieldVisited) {
				this.validateField(item);
			}
		});
	}

	/**
	 * Binds listeners to forms and fields
	 * @returns {undefined}
	 */
	_bindFormHandlers() {
		let
			buttons = this._getButtons();

		this.form.addEventListener('click', ev => {
			if (ev.target.tagName !== 'A' && this.editable && this.editOnClick && !this.form.classList.contains('active')) {
				ev.stopPropagation();

				if (this._getFields(':not([data-readonly])').length) {
					this.edit();
				}
			}
		});

		this.form.addEventListener('submit', ev => {
			ev.preventDefault();
			ev.stopPropagation();
			this.save(true);
		});

		if (buttons.edit) {
			buttons.edit.addEventListener('click', ev => {
				if (this.editable) {
					ev.preventDefault();
					ev.stopPropagation();
					this.edit();
				}
			});
		}

		if (buttons.save) {
			buttons.save.addEventListener('click', ev => {
				ev.preventDefault();
				ev.stopPropagation();
				this.save(true);
			});
		}

		if (buttons.cancel) {
			buttons.cancel.addEventListener('click', ev => {
				ev.preventDefault();
				ev.stopPropagation();
				this.cancel(true);
			});
		}

		if (buttons.delete) {
			buttons.delete.addEventListener('click', ev => {
				ev.preventDefault();
				ev.stopPropagation();

				if (this.handlers.delete) {
					this.handlers.delete.call(this);
				}

				this.showLoadingMask();
			});
		}

		this._getFields().forEach(field => {
			field.addEventListener('input', this._onInput.bind(this));

			if (this.validatePreviousOnFocus) {
				field.addEventListener('focus', ev => {
					this._validatePreviousFields(ev.currentTarget);
				});
			}

			if (this.validateOnBlur) {
				field.addEventListener('blur', ev => {
					let
						fields = this._getFields(':not([readonly])');

					// validate form when leaving the last field
					if (ev.currentTarget === fields[fields.length - 1]) {
						this.validateForm();
					}

					// validate only if there's any value to not bother user during walking through empty fields
					if (ev.currentTarget.value !== undefined) {
						this.validateField(ev.currentTarget);
					}
				});
			}

			if (['radio', 'checkbox'].includes(field.type)) {
				field.addEventListener('click', ev => {
					if (ev.currentTarget.getAttribute('readonly') === 'readonly') {
						ev.preventDefault();
					}
				});
			}
		});

		this._getFields(undefined, 'select').forEach(field => {
			field.addEventListener('keydown', ev => {
				// ENTER
				if (ev.keyCode === 13) {
					ev.preventDefault();
					this.save(true);
				}
			});
		});

		document.addEventListener('keydown', ev => {
			// ESC
			if (ev.keyCode === 27 && this.form.contains(ev.target)) {
				ev.preventDefault();
				ev.stopPropagation();
				this.cancel(true);
			}
		});

		window.addEventListener('beforeunload', () => {
			if (this.dirty) {
				return this._translate('You have unsaved changes, do you really want to leave?');
			}
		});
	}

	/**
	 * @param {Event} ev
	 * @returns {undefined}
	 */
	_onInput(ev) {
		let
			field = ev.currentTarget,
			fieldName = field.name,
			labels = this._getLabels(field);

		delete this.validatedValues[fieldName];
		field.classList.remove('valid');
		field.classList.remove('invalid');

		if (labels) {
			labels.forEach(label => {
				label.classList.remove('valid');
				label.classList.remove('invalid');
			});
		}

		if (this.handlers.change) {
			this.handlers.change.call(this, field);
		}
		this.dirty = true;
	}

	/**
	 * @param {string} [selector]
	 * @param {string} [preSelector]
	 * @returns {Element[]}
	 */
	_getFields(selector, preSelector) {
		let
			subFormFields = [],
			subForms = Array.from(this.form.querySelectorAll('form')),
			fields = Array.from(this.form.querySelectorAll((preSelector || '') + '.field' + (selector || '')));

		subForms.forEach(form => {
			let
				formFields = Array.from(form.querySelectorAll('.field'));

			subFormFields = subFormFields.concat(formFields);
		});

		return fields.filter(field => {
			return !subFormFields.includes(field);
		});
	}

	/**
	 * Returns button elements
	 * @returns {Object}
	 */
	_getButtons() {
		return {
			edit: this.form.querySelector('.edit-button'),
			save: this.form.querySelector('.save-button'),
			cancel: this.form.querySelector('.cancel-button'),
			delete: this.form.querySelector('.delete-button')
		};
	}

	/**
	 * Returns all field labels based on 'for' and 'id' attributes
	 * @param  {Element} field
	 * @returns {Element[]}
	 */
	_getLabels(field) {
		let
			labels = [],
			fields = this._getFields('[name="' + field.getAttribute('name') + '"]');

		fields.forEach(field => {
			let
				fieldId = field.getAttribute('id');

			this.form.querySelectorAll('label[for="' + fieldId + '"]').forEach(label => {
				labels.push(label);
			});
		});

		return labels;
	}

	/**
	 * Sets initial data based on DOM values
	 * @param {Object} [data] overrides DOM values
	 * @returns {undefined}
	 */
	_setInitialData(data) {
		assert.type(data, '?{}');

		let
			initialData = this.getData();

		Object.assign(initialData, data || {});
		this.setData(initialData);
	}

	/**
	 * Deep object property comparison
	 * @param  {Object} a
	 * @param  {Object} b
	 * @param  {boolean} strict strict value equality
	 * @returns {boolean}
	 */
	_compareObjects(a, b, strict) {
		assert.type(a, '{}');
		assert.type(b, '{}');
		assert.type(strict, '?boolean');

		let
			equals = false,
			keyListA = Object.keys(a).sort().join(),
			keyListB = Object.keys(b).sort().join();

		if (keyListA === keyListB) {
			if (keyListA === '') {
				equals = true;
			}
			else {
				for (let key in a) {
					if (a[key] instanceof FileList) {
						equals = a[key].length === b[key].length;
					}
					else if (a[key] && typeof a[key] === 'object') {
						if (typeof b[key] === 'object') {
							equals = this._compareObjects(a[key], b[key], strict);
						}
						else {
							equals = false;
						}

						if (!equals) {
							break;
						}
					}
					else if (strict) {
						equals = (a[key] === b[key]);

						if (!equals) {
							console.warn('Key', key, 'is not strict equal');
							break;
						}
					}
					else {
						equals = (a[key] == b[key]);

						if (!equals) {
							console.warn('Key', key, 'is not equal');
							break;
						}
					}
				}
			}
		}
		else {
			console.warn('Objects have different key list:');
			console.warn(keyListA);
			console.warn(keyListB);
		}

		return equals;
	}

	/**
	 * Translates a string using external translate function, if any
	 * @param {string} text
	 * @returns {string}
	 */
	_translate(text) {
		if (typeof this.translate === 'function') {
			return this.translate(text);
		}
		else {
			return text;
		}
	}
}

Form.prototype.VALIDATORS = {
	default: function() {
		return {
			valid: true
		};
	},
	required: function(value) {
		return {
			valid: !!(value && value.length),
			hint: this._translate('This field is required')
		};
	},
	email: function(value) {
		return {
			valid: !value || /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,63}$/.test(value),
			hint: this._translate('E-mail value has wrong format')
		};
	},
	passwordConfirm: function(value) {
		return {
			valid: value.password === value.passwordConfirm,
			hint: this._translate('Both passwords have to be equal')
		};
	},
	checked: function(value) {
		assert.type(value, 'boolean');

		return {
			valid: value,
			hint: this._translate('This field has to be checked')
		};
	},
	unchecked: function(value) {
		assert.type(value, 'boolean');

		return {
			valid: !value,
			hint: this._translate('This field has to be unchecked')
		};
	}
};

exports.Form = Form;
